from utils.data_sanitizers import DataSanitizer
from utils.data_classes import Event
from utils.providers import CSVProvider
from utils.utils import DataRow


class EventBuilder:

    def __init__(self, data_list: list, line: int, track: str):
        self.data_list = data_list
        self.line = line
        self.track = track
        self.file_header = ['evt_code', 'evt_desc', 'pk_geo', 'pk_client', 'line', 'track', 'family', 'pd_show',
                            'graph_show', 'created_by', 'data_version']

        self.sanitizer = DataSanitizer()
        self.csv_provider = CSVProvider()

    def build(self) -> None:
        event_data: dict = {float(data[DataRow.PK_ROW.value]) * 1000: Event(self.sanitizer.event_sanitizer(
            data[DataRow.EVENT_ROW.value]), int(data[DataRow.SHOW_ROW.value]))
            for data in self.data_list if data[DataRow.EVENT_ROW.value] and data[DataRow.EVENT_ROW.value] != ' '}
        print('event_data ready')
        self.__create_event_csv_file(event_data)

    def __create_event_csv_file(self, event_data: dict) -> None:
        csv_data = []
        [csv_data.append(self.__get_csv_row(pk, data)) for pk, data in event_data.items()]
        print('setted up event_data')
        print('creating event csv file')
        self.csv_provider.create('events', [self.file_header, *csv_data], self.line, self.track)
        print('event csv file created')

    def __get_csv_row(self, pk: float, data: Event) -> list:
        return [data.code, data.code, round(pk, 2), round((pk * 1000), 2), self.line, self.track,
                self.__get_evt_family(data.code), self.__is_showed_in_pd(data.show),
                self.__is_showed_in_graph(data.code), DataRow.CREATED_BY.value, DataRow.DATA_VERSION.value]

    @staticmethod
    def __get_evt_family(event: str) -> str:
        return 'Senalizacion' if '+' in event or event[0:3] == 'SE ' else 'Via'

    @staticmethod
    def __is_showed_in_pd(show: int) -> bool:
        if show:
            return True
        return False

    @staticmethod
    def __is_showed_in_graph(event: str) -> bool:
        if event[:3] in ['PN ', 'TC ', 'JCA', 'AND']:
            return True
        return False
