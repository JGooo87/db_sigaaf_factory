from math import floor
from typing import Dict

from utils.data_classes import GeometryCoordinates
from utils.providers import CSVProvider
from utils.utils import DataRow, GeoTool


class QIGeometryBuilder:

    def __init__(self, data_list: list, line: int, track: str):
        self.data_list = data_list
        self.line = line
        self.track = track
        self.file_header = [['code', 'line', 'track', 'data_version', 'current', 'geometry']]
        self.max_pk = 0

        self.tools = GeoTool()
        self.csv_provider = CSVProvider()

    def build(self) -> None:
        setted_up_data = self.__set_up_data(self.data_list)
        self.max_pk = max(setted_up_data.keys())
        self.__create_qi_geometry_csv_file(setted_up_data)

    @staticmethod
    def __set_up_data(data_list: list) -> dict:
        qi_geometry_data: Dict[float, GeometryCoordinates] = {}
        [qi_geometry_data.update(
            {float(data[DataRow.PK_ROW.value]) * 1000: GeometryCoordinates(float(data[DataRow.X_ROW.value]),
                                                                           float(data[DataRow.Y_ROW.value]))}
        ) for data in data_list]
        print('qi_geometry_data ready')
        return qi_geometry_data

    def __create_qi_geometry_csv_file(self, qi_data: dict) -> None:
        print('setting up qi_geometry_data')
        coord_stretch50, coord_stretch100, coord_stretch200 = [], [], []
        csv_stretch50, csv_stretch100, csv_stretch200 = [], [], []
        for pk, coord in qi_data.items():
            pk_inf = round(floor(pk / 100), 1) * 100
            coord_stretch50, csv_stretch50 = self.__set_data_list(50, pk, pk_inf, coord, coord_stretch50, csv_stretch50)
            coord_stretch100, csv_stretch100 = self.__set_data_list(100, pk, pk_inf, coord, coord_stretch100,
                                                                    csv_stretch100)
            coord_stretch200, csv_stretch200 = self.__set_data_list(200, pk, pk_inf, coord, coord_stretch200,
                                                                    csv_stretch200)

        print('setted up qi_geometry_data')
        print('creating qi_geometry csv files')
        self.csv_provider.create(f'qi_{50}', [self.file_header, *csv_stretch50], self.line, self.track)
        self.csv_provider.create(f'qi_{100}', [self.file_header, *csv_stretch100], self.line, self.track)
        self.csv_provider.create(f'qi_{200}', [self.file_header, *csv_stretch200], self.line, self.track)
        print('qi_geometry csv files created')

    def __set_data_list(self, stretch, pk, pk_inf, coord, coord_stretch, csv_stretch) -> tuple:
        pk_inf_stretch, pk_sup_stretch = self.__get_pks(stretch, pk, pk_inf)
        if pk_inf_stretch < pk < pk_sup_stretch:
            coord_stretch.append([coord.x, coord.y])
        else:
            if coord_stretch:
                coord_stretch = self.tools.add_final_line_points(coord_stretch)
                pk_inf_stretch = pk_inf_stretch - stretch
                pk_sup_stretch = pk_sup_stretch - stretch
                csv_stretch.append(self.__get_csv_row(stretch, pk_inf_stretch, pk_sup_stretch, coord_stretch))
                coord_stretch = [[coord.x, coord.y]]

        if pk == self.max_pk:
            coord_stretch = self.tools.add_final_line_points(coord_stretch)
            pk_inf_stretch = pk_inf_stretch
            pk_sup_stretch = int(self.max_pk)
            csv_stretch.append(self.__get_csv_row(stretch, pk_inf_stretch, pk_sup_stretch, coord_stretch))

        return coord_stretch, csv_stretch

    def __get_pks(self, stretch: int, pk: float, pk_inf: float) -> tuple:
        if stretch == 50:
            return self.__get_pks_stretch50(pk, pk_inf)
        if stretch == 100:
            return self.__get_pks_stretch100(pk_inf)
        return self.__get_pks_stretch200(pk, pk_inf)

    @staticmethod
    def __get_pks_stretch50(pk: float, pk_inf: float) -> tuple:
        pk_inf_stretch50 = pk_inf
        if pk_inf_stretch50 + 50 <= pk:
            pk_inf_stretch50 = pk_inf_stretch50 + 50
        pk_sup_stretch50 = pk_inf_stretch50 + 50
        return pk_inf_stretch50, pk_sup_stretch50

    @staticmethod
    def __get_pks_stretch100(pk_inf: float) -> tuple:
        return pk_inf, pk_inf + 100

    @staticmethod
    def __get_pks_stretch200(pk: float, pk_inf: float) -> tuple:
        pk_inf_stretch200 = pk_inf
        if pk_inf_stretch200 % 200 != 0 and pk - pk_inf_stretch200 <= 100:
            pk_inf_stretch200 = pk_inf_stretch200 - 100
        pk_sup_stretch200 = pk_inf_stretch200 + 200
        return pk_inf_stretch200, pk_sup_stretch200

    def __get_csv_row(self, stretch: int, pk_inf_stretch: float, pk_sup_stretch: float, coord_stretch: list) -> list:
        return [f'stretch_{stretch}-L{self.line}-V{self.track}-{pk_inf_stretch * 100000}-{pk_sup_stretch * 100000}',
                self.line, self.track, 0, True, {"type": "MultiLineString", "coordinates": [coord_stretch]}]
