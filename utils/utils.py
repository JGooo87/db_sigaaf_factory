from math import sin, cos, atan2, pi
from enum import Enum

from pyproj import Transformer
from pyproj.aoi import AreaOfInterest


class DataRow(Enum):
    PK_ROW = 0
    EVENT_ROW = 16
    RAIL_TYPOLOGY_CODE_ROW = 17
    TECHNICAL_LOCATION_ROW = 20
    RAIL_TOPOLOGY_ROW = 21
    ARMAMENT_ROW = 22
    FASTENING_ROW = 23
    GEOMETRY_TYPE_ROW = 24
    X_ROW = 25
    Y_ROW = 26
    RADIUS_ROW = 28
    ARROW_ROW = 29
    THEORETICAL_WIDTH_ROW = 30
    RADIUS_WIDTH_ROW = 31
    USE_WIDTH_ROW = 32
    CIRCULATION_SPEED_ROW = 33
    PROJECT_CANT_ROW = 34
    INTER_STATION_ROW = 35
    SHOW_ROW = 36
    CREATED_BY = 'jgomez'
    HUSO = 30
    DATA_VERSION = 0
    RIGHT_LEVELING_ROW = 1
    LEFT_LEVELING_ROW = 2
    DYNAMICAL_CANT_ROW = 3
    QS_CANT_ROW = 4
    CANT_ROW = 5
    RIGHT_DYNAMICAL_ALIGNMENT_ROW = 6
    LEFT_DYNAMICAL_ALIGNMENT_ROW = 7
    RIGHT_ALIGNMENT_ROW = 8
    LEFT_ALIGNMENT_ROW = 9
    DYNAMICAL_WIDTH_ROW = 10
    WIDTH_ROW = 11
    WIDTH_PER_METER_ROW = 12
    TWIST_ROW = 13
    AUSCULTATION_DATE_ROW = 14

    @classmethod
    def values(cls):
        return tuple(i.value for i in cls)

    @classmethod
    def items(cls):
        return tuple((i.name, i.value) for i in cls)

    @classmethod
    def description(cls):
        return tuple((i.value, i.name) for i in cls)


class GeoTool:
    def add_final_line_points(self, stretch_list: list) -> list:
        xu, yu = stretch_list[-1]
        xp, yp = stretch_list[-2]

        azimut = self.__calculate_azimut(xu, yu, xp, yp)

        xadd = xu + 0.5 * sin(azimut - (pi / 2))
        yadd = yu + 0.5 * cos(azimut - (pi / 2))

        xadd2 = xu + 0.5 * sin(azimut + (pi / 2))
        yadd2 = yu + 0.5 * cos(azimut + (pi / 2))

        stretch_list.append([xadd, yadd])
        stretch_list.append([xadd2, yadd2])

        x_coord_list, y_coord_list = self.__split_list_coord(stretch_list)

        return self.__get_utm_2_geo_coordinate_lists(x_coord_list, y_coord_list)

    @staticmethod
    def __calculate_azimut(x1, y1, x2, y2) -> float:
        dx = x2 - x1
        dy = y2 - y1

        az = atan2(dx, dy)
        if az < 0.0:
            az += 2.0 * pi

        return az

    @staticmethod
    def __split_list_coord(coord_list: list) -> tuple:
        x_coord_list, y_coord_list = [], []
        for crds in coord_list:
            x_coord_list.append(crds[0])
            y_coord_list.append(crds[1])
        return x_coord_list, y_coord_list

    def __get_utm_2_geo_coordinate_lists(self, x_list: list, y_list: list) -> list:
        # UTM 30N WGS84 -> 25830
        # GEO WGS84 -> 4326
        # Valencian Community AOI -> -1.5º, 37.5º, 1.5º, 41º
        transformer = Transformer.from_crs(25830, 4326, area_of_interest=AreaOfInterest(-1.5, 37.5, 1.5, 41))
        lat_list, lon_list = transformer.transform(x_list, y_list)
        return self.__merge_coord_list(lat_list, lon_list)

    @staticmethod
    def __merge_coord_list(lat_list: list, lon_list: list) -> list:
        merged_list = []
        for i in range(len(lat_list)):
            merged_list.append([lat_list[i], lon_list[i]])
        return merged_list
