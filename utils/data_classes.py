from dataclasses import dataclass


@dataclass
class GeometryCoordinates:
    x: float
    y: float


@dataclass
class Event:
    code: str
    show: int


@dataclass
class RailDetail:
    pk_client: float
    line: int
    track: str
    category: str
    direction: str
    rail_typology_code: str
    armament: str
    fastening: str
    rail_typology: str
    circulation_speed: str
    technical_location: str
    inter_station: str
    x: float
    y: float
    created_by: str
    data_version: int
    huso: int


@dataclass
class Tracing:
    pk_client: float
    line: int
    track: str
    geometry_type: str
    radius: str
    arrow: str
    theoretical_width: str
    radius_width: str
    use_width: str
    project_cant: str
    created_by: str
    data_version: int


@dataclass
class GeometricalAuscultation:
    pk_client: float
    right_leveling: str
    left_leveling: str
    dynamical_cant: str
    qs_cant: str
    cant: str
    right_dynamical_alignment: str
    left_dynamical_alignment: str
    right_alignment: str
    left_alignment: str
    dynamical_width: str
    width: str
    width_per_meter: str
    twist: str
    auscultation_date: str
