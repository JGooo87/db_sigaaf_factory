from typing import Dict

from utils.data_classes import Tracing
from utils.providers import CSVProvider
from utils.utils import DataRow


class TracingBuilder:

    def __init__(self, data_list: list, line: int, track: str):
        self.data_list = data_list
        self.line = line
        self.track = str(track)

        self.file_header = ['pk_geo', 'pk_client', 'line', 'track', 'geometry_type', 'radius', 'arrow',
                            'theoretical_width', 'radius_width', 'use_width', 'project_cant', 'created_by',
                            'data_version']

        self.csv_provider = CSVProvider()

    def build(self) -> None:
        tracing_data: Dict[float, Tracing] = {}
        [tracing_data.update(
            {float(data[DataRow.PK_ROW.value]) * 1000: Tracing(
                float(data[DataRow.PK_ROW.value]), self.line, self.track, data[DataRow.GEOMETRY_TYPE_ROW.value],
                data[DataRow.RADIUS_ROW.value], data[DataRow.ARROW_ROW.value],
                data[DataRow.THEORETICAL_WIDTH_ROW.value], data[DataRow.RADIUS_WIDTH_ROW.value],
                data[DataRow.USE_WIDTH_ROW.value], data[DataRow.PROJECT_CANT_ROW.value], DataRow.CREATED_BY.value,
                DataRow.DATA_VERSION.value)}
        ) for data in self.data_list]
        print('tracing_data ready')
        self.__create_tracing_csv_file(tracing_data)

    def __create_tracing_csv_file(self, tracing_data: dict) -> None:
        csv_data = []
        [csv_data.append(self.__get_csv_row(pk, data)) for pk, data in tracing_data.items()]
        print('setted up tracing_data')
        print('creating tracing_data csv file')
        self.csv_provider.create('tracing', [self.file_header, *csv_data], self.line, self.track)
        print('tracing_data csv file created')

    def __get_csv_row(self, pk: float,  data: Tracing) -> list:
        return [round(pk, 2), round((pk * 1000), 2), self.line, self.track, data.geometry_type,
                data.radius, data.arrow, data.theoretical_width, data.radius_width, data.use_width,
                data.project_cant, data.created_by, data.data_version]

