from typing import Dict

from utils.data_classes import RailDetail
from utils.data_sanitizers import DataSanitizer
from utils.providers import CSVProvider
from utils.utils import DataRow


class RailDetailBuilder:

    def __init__(self, data_list: list, line: int, track: str):
        self.data_list = data_list
        self.line = line
        self.track = str(track)
        self.direction = 'D' if track == 2 else 'A'
        self.category = 'General' if track == 1 or track == 2 else 'Apartado'

        self.file_header = ['pk_geo', 'pk_client', 'line', 'track', 'category', 'direction', 'rail_typology_code',
                            'armament', 'fastening', 'rail_typology', 'circulation_speed', 'technical_location_code',
                            'inter_station_code', 'x', 'y', 'created_by', 'data_version', 'huso']

        self.sanitizer = DataSanitizer()
        self.csv_provider = CSVProvider()

    def build(self) -> None:
        rail_detail_data: Dict[float, RailDetail] = {}
        [rail_detail_data.update(
            {float(data[DataRow.PK_ROW.value]) * 1000: RailDetail(
                float(data[DataRow.PK_ROW.value]), self.line, self.track, self.category, self.direction,
                self.sanitizer.rail_typology_code_sanitizer(data[DataRow.RAIL_TYPOLOGY_CODE_ROW.value]),
                data[DataRow.ARMAMENT_ROW.value], data[DataRow.FASTENING_ROW.value],
                data[DataRow.RAIL_TOPOLOGY_ROW.value], data[DataRow.CIRCULATION_SPEED_ROW.value],
                data[DataRow.TECHNICAL_LOCATION_ROW.value], data[DataRow.INTER_STATION_ROW.value],
                float(data[DataRow.X_ROW.value]), float(data[DataRow.Y_ROW.value]), DataRow.CREATED_BY.value,
                DataRow.DATA_VERSION.value, DataRow.HUSO.value)}
        ) for data in self.data_list]
        print('rail_detail_data ready')
        self.__create_rail_detail_csv_file(rail_detail_data)

    def __create_rail_detail_csv_file(self, rail_detail_data: dict) -> None:
        csv_data = []
        [csv_data.append(self.__get_csv_row(pk, data)) for pk, data in rail_detail_data.items()]
        print('setted up rail_details_data')
        print('creating rail_details csv file')
        self.csv_provider.create('rail_details', [self.file_header, *csv_data], self.line, self.track)
        print('rail_details csv file created')

    def __get_csv_row(self, pk: float,  data: RailDetail) -> list:
        return [round(pk, 2), round((pk * 1000), 2), self.line, self.track, data.category,
                data.direction, data.rail_typology_code, data.armament, data.fastening, data.rail_typology,
                data.circulation_speed, data.technical_location, data.inter_station, round(data.x, 3), round(data.y, 3),
                data.created_by, data.data_version, data.huso]
