from factories.data_factory import DataFactory
from utils.providers import CSVProvider

if __name__ == "__main__":
    line = 2
    track = '1'
    file = f'V_AG2021_P_L{line}_V{track}.csv'

    print('Reading file')
    data_list = CSVProvider().read(file)

    DataFactory(data_list, line, track)
    print('Done')
