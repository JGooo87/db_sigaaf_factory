from os import path, makedirs
import csv


class CSVProvider:
    @staticmethod
    def read(file: str) -> list:
        with open(file, 'r', encoding='ISO-8859-1') as data:
            data_list = [data_line.split(';') for data_line in data.readlines()][1:-1]
        return data_list

    @staticmethod
    def create(code: str, csv_list: list, line: int, track: str) -> None:
        directory = './results'
        if not path.exists(directory):
            makedirs(directory)
        with open(f'{directory}/{code}_L{line}_V{track}.csv', 'w', encoding='ISO-8859-1') as qi_file:
            writer = csv.writer(qi_file)
            writer.writerows(csv_list)
