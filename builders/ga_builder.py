from typing import Dict

from utils.data_classes import GeometricalAuscultation
from utils.providers import CSVProvider
from utils.utils import DataRow


class GeometricalAuscultationBuilder:

    def __init__(self, data_list: list, line: int, track: str):
        self.data_list = data_list
        self.line = line
        self.track = str(track)

        self.file_header = ['pk_geo', 'pk_client', 'right_leveling', 'left_leveling', 'dynamical_cant', 'qs_cant',
                            'cant', 'right_dynamical_alignment', 'left_dynamical_alignment', 'right_alignment',
                            'left_alignment', 'dynamical_width', 'width', 'width_per_meter', 'twist',
                            'auscultation_date', 'line', 'track']

        self.csv_provider = CSVProvider()

    def build(self) -> None:
        ga_data: Dict[float, GeometricalAuscultation] = {}
        [ga_data.update(
            {float(data[DataRow.PK_ROW.value]) * 1000: GeometricalAuscultation(
                float(data[DataRow.PK_ROW.value]), data[DataRow.RIGHT_LEVELING_ROW.value],
                data[DataRow.LEFT_LEVELING_ROW.value], data[DataRow.DYNAMICAL_CANT_ROW.value],
                data[DataRow.QS_CANT_ROW.value], data[DataRow.CANT_ROW.value],
                data[DataRow.RIGHT_DYNAMICAL_ALIGNMENT_ROW.value], data[DataRow.LEFT_DYNAMICAL_ALIGNMENT_ROW.value],
                data[DataRow.RIGHT_ALIGNMENT_ROW.value], data[DataRow.LEFT_ALIGNMENT_ROW.value],
                data[DataRow.DYNAMICAL_WIDTH_ROW.value], data[DataRow.WIDTH_ROW.value],
                data[DataRow.WIDTH_PER_METER_ROW.value], data[DataRow.TWIST_ROW.value],
                data[DataRow.AUSCULTATION_DATE_ROW.value])}
        ) for data in self.data_list]
        print('ga_data ready')
        self.__create_ga_csv_file(ga_data)

    def __create_ga_csv_file(self, ga_data: dict) -> None:
        csv_data = []
        [csv_data.append(self.__get_csv_row(pk, data)) for pk, data in ga_data.items()]
        print('setted up ga_data')
        print('creating ga_data csv file')
        self.csv_provider.create('GA', [self.file_header, *csv_data], self.line, self.track)
        print('ga_data csv file created')

    def __get_csv_row(self, pk: float,  data: GeometricalAuscultation) -> list:
        return [round(pk, 2), round((pk * 1000), 2), data.right_leveling, data.left_leveling, data.dynamical_cant,
                data.qs_cant, data.cant, data.right_dynamical_alignment, data.left_dynamical_alignment,
                data.right_alignment, data.left_alignment, data.dynamical_width, data.width, data.width_per_meter,
                data.twist, data.auscultation_date, self.line, self.track]

