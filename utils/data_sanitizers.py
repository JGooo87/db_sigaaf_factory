class DataSanitizer:

    @staticmethod
    def event_sanitizer(event: str) -> str:
        if event[0] == ' ':
            event = event[1:]
        event_sanitized = event.replace(' II', '').replace('  FF', '')
        if event_sanitized[-2:] == ' I' or event_sanitized[-2:] == ' F':
            return event_sanitized.replace(' I', '').replace(' F', '')
        return event_sanitized

    @staticmethod
    def rail_typology_code_sanitizer(code: str) -> str:
        sanitized_code = code
        if code[0:3] == 'ES ':
            sanitized_code = 'ES'
        if code[0:3] == 'P1 ':
            sanitized_code = 'P1'
        if code[0:3] == 'P2 ':
            sanitized_code = 'P2'
        if code[0:3] == 'D1 ':
            sanitized_code = 'D1'
        if code[0:3] == 'D2 ':
            sanitized_code = 'D2'
        if code[0:3] == 'D3 ':
            sanitized_code = 'D3'
        return sanitized_code
