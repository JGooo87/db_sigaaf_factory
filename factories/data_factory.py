from builders.event_builder import EventBuilder
from builders.ga_builder import GeometricalAuscultationBuilder
from builders.qi_geometry_builder import QIGeometryBuilder
from builders.rail_detail_builder import RailDetailBuilder
from builders.tracing_builder import TracingBuilder


class DataFactory:
    def __init__(self, data_list: list, line: int, track: str):
        data_builders = [QIGeometryBuilder(data_list, line, track), EventBuilder(data_list, line, track),
                         RailDetailBuilder(data_list, line, track), TracingBuilder(data_list, line, track),
                         GeometricalAuscultationBuilder(data_list, line, track)]
        print('extracting data')
        list(map(lambda builder: builder.build(), data_builders))
